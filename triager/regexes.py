"""Regexes to look for bugs 🐛🐛."""
import re

from cki_lib.logger import get_logger

import settings

from . import dw_client

LOGGER = get_logger('cki.triager.regexes')


class RegexChecker:
    # pylint: disable=too-few-public-methods
    """RegexChecker."""

    def __init__(self):
        """Init."""
        self.lookups = []

    @staticmethod
    def _compile_lookups(lookups):
        """Compile the regexes."""
        LOGGER.debug('Compiling lookups')
        compiled_lookups = []
        for lookup in lookups:
            for field_name in ('text_match', 'test_name_match', 'file_name_match'):
                field = getattr(lookup, field_name)
                compiled_field = re.compile(f'.*{field}.*', re.DOTALL) if field else None
                setattr(lookup, f'compiled_{field_name}', compiled_field)

            compiled_lookups.append(lookup)

        return compiled_lookups

    def download_lookups(self):
        """Download and compile regexes from Datawarehouse."""
        LOGGER.debug('Downloading lookups')
        self.lookups = self._compile_lookups(
            dw_client.issue_regex.list()
        )
        LOGGER.debug('Found %i lookups', len(self.lookups))

    def search(self, text, log, test):
        """Search for regexes ocurrences on text."""
        issues = []
        LOGGER.debug('Searching in %s', log['url'])
        for lookup in self.lookups:
            text_match = lookup.compiled_text_match
            test_name_match = lookup.compiled_test_name_match
            file_name_match = lookup.compiled_file_name_match

            if test_name_match and not test_name_match.match(test.description):
                continue

            if file_name_match and not file_name_match.match(log['name']):
                continue

            if not text_match.match(text):
                continue

            # Fallback. Found it.
            result = {'name': lookup.issue['description'], 'id': lookup.issue['id']}
            LOGGER.debug('Found %s', result)
            issues.append(result)

        return issues or settings.NOT_FOUND
