"""Triager checkers."""
from cki_lib.logger import get_logger

from settings import BEAKER_URL, FAIL_KICKSTART, FAIL_WATCHDOG_EXPIRED
from triager import beaker, regexes

from . import session

LOGGER = get_logger('cki.triager.checkers')


class TestFailureChecker:
    """TestFailureChecker Class."""

    check_functions = (
        'check_kickstart_error',
        'check_logs_with_regex',
        'check_beaker_watchdogs',
    )

    def __init__(self, test):
        self.test = test

    @classmethod
    def check(cls, test):
        """Perform checks and return result."""
        checker = cls(test)
        return checker.check_all()

    def check_all(self):
        """Run the checks."""
        failures = []
        for function in self.check_functions:
            LOGGER.info(' running: %s', function)
            result = getattr(TestFailureChecker, function)(self)
            LOGGER.info('  result: %s', result)
            if result:
                failures.extend(result)

        LOGGER.info(' overall result: %s', failures)
        return failures

    def check_kickstart_error(self):
        """
        Check job failed to Kickstart.

        If 'Boot test' has no duration, failed to provision.
        """
        if self.test.description == 'Boot test' and not self.test.duration:
            return [FAIL_KICKSTART]

        return []

    def check_logs_with_regex(self):
        """Use regexes to find failures."""
        checker = regexes.RegexChecker()
        checker.download_lookups()

        failures = []
        for file in self.test.output_files:

            if not file['name'].endswith('.log'):
                # Not a log file.
                continue

            log_content = str(session.get(file['url']).content)
            failure = checker.search(log_content, file, self.test)

            if failure:
                failures.extend(failure)

        return failures

    def check_beaker_watchdogs(self):
        """Check if beaker watchdogs expired."""
        beaker_checker = beaker.BeakerChecker(BEAKER_URL)

        if not self.test.misc.get('beaker'):
            # Not a BeakerTestRun.
            return []

        if beaker_checker.check_testrun(self.test):
            return [FAIL_WATCHDOG_EXPIRED]

        return []
