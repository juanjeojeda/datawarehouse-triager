datawarehouse-api-lib @ git+https://gitlab.com/cki-project/datawarehouse-api-lib.git
cki-lib @ git+https://gitlab.com/cki-project/cki-lib.git
flake8
pydocstyle
pylint
mock
coverage
sentry-sdk

responses
